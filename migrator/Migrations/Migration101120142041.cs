﻿namespace px.Migrator.Migrations
{
    public class Migration101120142041 : Migration
    {
        public override long Id
        {
            get { return 101120142041; }
        }

        public override void MigrationScenario()
        {
            Execute(@"
alter table posts add column tagtext text;

update posts set tagtext = 'trance' where tag=1;
update posts set tagtext = 'deep house' where tag=2;
update posts set tagtext = 'minimal / techno' where tag=3;
update posts set tagtext = 'dnb / breaks' where tag=5;
update posts set tagtext = 'dubstep' where tag=6;
update posts set tagtext = 'electronic' where tag=7;
update posts set tagtext = 'rap / hip-hop' where tag=8;
update posts set tagtext = 'pop' where tag=9;
update posts set tagtext = 'reggae / dub' where tag=10;
update posts set tagtext = 'tech house' where tag=11;
update posts set tagtext = 'trap' where tag=12;
update posts set tagtext = 'edm / dance' where tag=13;
update posts set tagtext = 'classical' where tag=14;
update posts set tagtext = 'rock' where tag=16;
update posts set tagtext = 'speech / podcast' where tag=19;
update posts set tagtext = 'nu disco' where tag=21;
update posts set tagtext = 'jazz / blues' where tag=22;
update posts set tagtext = 'house / clubhouse' where tag=23;
update posts set tagtext = 'progressive / tribal' where tag=25;
update posts set tagtext = 'ambient / chillout' where tag=26;
update posts set tagtext = 'uk garage / london' where tag=28;
update posts set tagtext = 'funk / soul' where tag=29;
update posts set tagtext = 'trip hop' where tag=30;");
        }
    }
}