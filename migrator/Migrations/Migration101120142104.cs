﻿namespace px.Migrator.Migrations
{
    public class Migration101120142104 : Migration
    {
        public override long Id
        {
            get { return 101120142104; }
        }

        public override void MigrationScenario()
        {
            Execute("alter table posts add column tags text[];");
        }
    }
}