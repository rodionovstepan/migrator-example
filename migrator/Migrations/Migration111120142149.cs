﻿namespace px.Migrator.Migrations
{
    public class Migration111120142149 : Migration
    {
        public override long Id
        {
            get { return 111120142149; }
        }

        public override void MigrationScenario()
        {
            Execute(@"
update posts set tag = 2, tagtext = 'nu disco' where tag = 21;
update posts set tag = 23, tagtext = 'tech house' where tag = 11;
update users set favorites = array_remove(favorites, 11) where 11 = any(favorites);
update users set favorites = array_remove(favorites, 21) where 21 = any(favorites);
update users set favorites = array_remove(favorites, 27) where 27 = any(favorites);");
        }
    }
}