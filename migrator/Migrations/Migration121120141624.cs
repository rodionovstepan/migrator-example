﻿namespace px.Migrator.Migrations
{
    public class Migration121120141624 : Migration
    {
        public override long Id
        {
            get { return 121120141624; }
        }

        public override void MigrationScenario()
        {
            Execute("update posts set tags = array_append(tags, tagtext) where tagtext is not null;");
        }
    }
}