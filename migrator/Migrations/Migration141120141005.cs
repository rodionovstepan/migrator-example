﻿namespace px.Migrator.Migrations
{
    using System.Linq;
    using App.DataAccess;
    using App.Dtos;
    using App.Extensions;
    using App.Services;

    public class Migration141120141005 : Migration
    {
        public override long Id
        {
            get { return 141120141005; }
        }

        public override void MigrationScenario()
        {
            var tp = new HashtagProcessor();

            foreach (var post in Query<Post>("select * from posts where isdeleted = false"))
            {
                var tags = tp.GetTags(post.Description ?? string.Empty).ToArray();

                if (string.IsNullOrEmpty(post.TagText) == false)
                {
                    var tag = post.TagText.ComputeHash();

                    Execute(new QueryObject(
                        "update posts set hashtags = array_append(hashtags, @Tag) where id = @Id;",
                        new {Tag = tag, post.Id})
                        );
                }
                else if (tags.Length > 0)
                {
                    var tagtext = tags[0];
                    var tag = tagtext.ComputeHash();

                    Execute(new QueryObject(
                        "update posts set tagtext = @TagText, hashtags = array_append(hashtags, @Tag) where id = @Id;",
                        new {Tag = tag, TagText = tagtext, post.Id})
                        );
                }

                if (tags.Length > 0)
                {
                    Execute(new QueryObject(
                        "update posts set tags = array_cat(tags, @Tags) where id = @Id;",
                        new {Tags = tags, post.Id})
                        );
                }
            }
        }
    }
}