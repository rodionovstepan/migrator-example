﻿namespace px.Migrator.Migrations
{
    using System.Linq;
    using App.Dtos;

    public class Migration191120141921 : Migration
    {
        public override long Id
        {
            get { return 191120141921; }
        }

        public override void MigrationScenario()
        {
            foreach (var user in Query<User>("select id from users"))
            {
                var playlists = Query<Playlist>(
                    "select id, index from playlists where userid=@Id and isdeleted=false order by index",
                    user).ToArray();

                for (var i = 0; i < playlists.Length; ++i)
                {
                    Execute("update playlists set index = @Index where id = @Id",
                        new {Index = i + 1, playlists[i].Id});

                    var items = Query<PlaylistItem>(
                        "select id from playlistitems where playlistid=@Id and isdeleted = FALSE order by index",
                        playlists[i]).ToArray();

                    for (var j = 0; j < items.Length; ++j)
                    {
                        Execute("update playlistitems set index = @Index where id=@Id",
                            new {Index = j + 1, items[j].Id});
                    }
                }
            }
        }
    }
}