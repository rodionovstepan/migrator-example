﻿namespace px.Migrator.Migrations
{
    public class Migration221220141632 : Migration
    {
        public override long Id
        {
            get { return 221220141632; }
        }

        public override void MigrationScenario()
        {
            Execute("alter table playlists add column ispublic boolean not null default false");
        }
    }
}