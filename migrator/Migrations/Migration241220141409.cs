﻿namespace px.Migrator.Migrations
{
    public class Migration241220141409 : Migration
    {
        public override long Id
        {
            get { return 241220141409; }
        }

        public override void MigrationScenario()
        {
            Execute("alter table playlists add column imageurl text not null default ''");
        }
    }
}