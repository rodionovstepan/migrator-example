﻿namespace px.Migrator.Migrations
{
    public class Migration241220141748 : Migration
    {
        public override long Id
        {
            get { return 241220141748; }
        }

        public override void MigrationScenario()
        {
            Execute("update playlists set isdeleted = true where isdeleted = false and itemcount = 0");
        }
    }
}