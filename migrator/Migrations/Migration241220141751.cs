﻿namespace px.Migrator.Migrations
{
    using System.Linq;
    using App.Dtos;

    public class Migration241220141751 : Migration
    {
        public override long Id
        {
            get { return 241220141751; }
        }

        public override void MigrationScenario()
        {
            foreach (var playlist in Query<Playlist>("select * from playlists where isdeleted = false"))
            {
                var imageUrl =
                    Query<string>(
                        "select imageurl from posts where id = (select postid from playlistitems where playlistid=@Id order by index desc limit 1)",
                        new {playlist.Id}).FirstOrDefault();

                if (string.IsNullOrEmpty(imageUrl))
                    continue;

                Execute("update playlists set imageurl = @imageUrl where id = @Id", new {playlist.Id, imageUrl});
            }
        }
    }
}