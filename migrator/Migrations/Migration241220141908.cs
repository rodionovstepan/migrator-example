﻿namespace px.Migrator.Migrations
{
    public class Migration241220141908 : Migration
    {
        public override long Id
        {
            get { return 241220141908; }
        }

        public override void MigrationScenario()
        {
            Execute(@"
alter table playlists add column username text not null default '';
update playlists p set username = (select username from users where id = p.userid);");
        }
    }
}